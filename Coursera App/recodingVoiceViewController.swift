//
//  recodingVoiceViewController.swift
//  Coursera App
//
//  Created by User on 1/7/18.
//  Copyright © 2018 User. All rights reserved.
//

import UIKit
import AVFoundation
class recodingVoiceViewController: UIViewController , AVAudioRecorderDelegate {

    @IBOutlet weak var stopRecordingBtn: UIButton!
    @IBOutlet weak var recordBtn: UIButton!
    @IBOutlet weak var TapBtnTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        stopRecordingBtn.isEnabled = false;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var avRecorder : AVAudioRecorder!
    @IBAction func OnTapButtonClicked(_ sender: Any) {
        TapBtnTxt.text = "Recording in progress";
        stopRecordingBtn.isEnabled = true;
        recordBtn.isEnabled = false;
        
        let basePath = NSSearchPathForDirectoriesInDomains( .documentDirectory,.userDomainMask, true)[0] as String ;
        let recordingName = "recordedVoice.wav";
        let pathArray = [ basePath,recordingName]
        let filePath = URL(string: pathArray.joined(separator: "/"))
        let session = AVAudioSession.sharedInstance()
        try! session.setCategory(AVAudioSessionCategoryPlayAndRecord , with : AVAudioSessionCategoryOptions.defaultToSpeaker)
        try! avRecorder = AVAudioRecorder(url : filePath! , settings: [:])
        avRecorder.delegate = self
        avRecorder.isMeteringEnabled = true
        avRecorder.prepareToRecord()
        avRecorder.record()
        
    }
    
    @IBAction func OnStopRecordBtnClicked(_ sender: Any) {
        stopRecordingBtn.isEnabled = false;
        recordBtn.isEnabled = true;
        TapBtnTxt.text = "Tap To Record"
        avRecorder.stop()
        let session = AVAudioSession.sharedInstance()
        try! session.setActive(false)
        
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if (flag)
        {
             performSegue(withIdentifier: "stopRecording", sender: avRecorder.url)
        }
        else
        {
            print("recording was not sucessful")
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "stopRecording"){
            let playSoundVC = segue.destination as! PlaySoundViewController
            let recordedSoundUrl = sender as! URL
            playSoundVC.recordedVoiceURL = recordedSoundUrl
            print("prepared")
        }
        
    }
}

